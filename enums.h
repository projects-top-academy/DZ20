#pragma once

enum Movement
{
    vpered = 0,
    nazad,
    vlevo,
    vpravo
};

enum Difficulty
{
    easy = 0,
    normal = 1,
    hard = 2
};

enum Race
{
    Elf = 0,
    Goblin = 1,
    Human = 2
};

enum Weather
{
    sandstorm = 0,
    rain = 1,
    snow = 2,
    clear = 3
};

enum Map
{
    Desert = 0,
    Megapolis = 1,
    Forest = 2
};