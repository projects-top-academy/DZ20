#pragma once

#include "enums.h"

struct Player
{
    Race race;
    std::string Player_Name;
    int Achievement_level;
};

struct Character
{
    int health;
    Movement movement;
};

struct Maps
{
    Map map;
    Weather weather;
    Player player;
};